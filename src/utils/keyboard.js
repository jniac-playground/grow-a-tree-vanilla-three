
export const downKeys = new Set()

export let shift = false

window.addEventListener('keydown', event => {
  
  downKeys.add(event.key.toUpperCase())

  if (event.key === 'Shift') shift = true
})

window.addEventListener('keyup', event => { 
  
  downKeys.delete(event.key.toUpperCase())

  if (event.key === 'Shift') shift = false
})
