
export const toDegrees = 180 / Math.PI
export const toRadians = Math.PI / 180

/**
 * @param {number} scalar 
 * @param  {...number|number[]} args 
 */
export const scaleNumbers = (scalar, ...args) => {

  if (args.length === 1) {
    if (typeof args[0] === 'number') {
      return args[0] * scalar
    }
    if (Array.isArray(args[0])) {
      return args[0].map(n => n * scalar)
    }
  }

  return args.map(n => n * scalar)
}

export const degrees = (...args) => scaleNumbers(toDegrees, ...args)
export const radians = (...args) => scaleNumbers(toRadians, ...args)

export function* enumerate(max) {
  let i = 0
  while (i < max) {
    yield i++
  }
}