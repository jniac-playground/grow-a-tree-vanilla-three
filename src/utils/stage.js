import * as THREE from 'https://cdn.jsdelivr.net/npm/three@0.125.1/build/three.module.js'
import { OrbitControls } from 'https://cdn.jsdelivr.net/npm/three@0.125.1/examples/jsm/controls/OrbitControls.js'
import './keyboard.js'

// https://threejs.org/docs/index.html#manual/en/introduction/Creating-a-scene
const scene = new THREE.Scene()
const camera = new THREE.PerspectiveCamera(60, window.innerWidth / window.innerHeight, 0.1, 1000)
const renderer = new THREE.WebGLRenderer()

renderer.setPixelRatio(devicePixelRatio)
renderer.setSize(window.innerWidth, window.innerHeight)
document.body.appendChild(renderer.domElement)

camera.position.z = 5

const raycaster = new THREE.Raycaster()
const mouse = new THREE.Vector2()
const orbitControls = new OrbitControls(camera, renderer.domElement)

const textureLoader = new THREE.TextureLoader()
const textures = new Map()
const getTexture = url => {
	
	if (textures.has(url)) {
		return textures.get(url)
	}

	const texture = textureLoader.load(url)
	textures.set(url, texture)
	return texture
}

let autoPauseDelay = 1
let autoPauseTime = 0
const autoPauseReset = () => autoPauseTime = 0
const setAutoPauseDelay = value => autoPauseDelay = parseFloat(value)

const dt = 1 / 60
let time = 0
let frame = 0

const loop = function () {
	requestAnimationFrame(loop)
	
	autoPauseTime += dt

	if (autoPauseTime <= autoPauseDelay) {

		const args = { time, frame }
		scene.traverse(child => {
			if(typeof child.update === 'function') {
				child.update(args)
			}
		})

		renderer.render(scene, camera)
		time += dt
		frame++
	}
}

loop()

window.addEventListener('pointermove', event => {
	mouse.x = (event.clientX / window.innerWidth) * 2 - 1
	mouse.y = - (event.clientY / window.innerHeight) * 2 + 1
  raycaster.setFromCamera(mouse, camera)
})

window.addEventListener('pointermove', autoPauseReset, true)
window.addEventListener('keydown', autoPauseReset, true)
window.addEventListener('mousewheel', autoPauseReset, true)

export {
	scene,
	camera,
	renderer,
	raycaster,
	orbitControls,
	getTexture,
	
	autoPauseReset,
	setAutoPauseDelay,
}

export default {
	scene,
	camera,
	renderer,
	raycaster,
	orbitControls,
	getTexture,
	
	autoPauseReset,
	setAutoPauseDelay,
}