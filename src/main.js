import * as THREE from 'https://cdn.jsdelivr.net/npm/three@0.125.1/build/three.module.js'
import { InstancedBox } from './InstancedBox.js'
import { enumerate } from './utils/math.js'
import stage, { camera, scene, raycaster, orbitControls } from './utils/stage.js'
import { disc, pointLight, wireSphere } from './stuff.js'
import { downKeys, shift } from './utils/keyboard.js'

camera.position.set(24, 1, 11)
orbitControls.target.set(2, 10, 2)
orbitControls.update()

const tree = new InstancedBox()
for (const _ of enumerate(16))
  tree.grow(0)

scene.add(
  wireSphere({ radius:40, half:true }),
  new THREE.AmbientLight('white', 0.5),
  pointLight({ position:[20, 30, 10], color:'white' }),
  disc({ radius:10 }),
  tree,
)

window.addEventListener('click', () => {
  console.time('raycast')
  const intersections = []
  tree.raycast(raycaster, intersections)
  console.timeEnd('raycast')
  if (intersections.length === 0) {
    tree.setBranchColor(0, 'white')
  } else {
    const { instanceId } = intersections[0]
    tree.setBranchColor(instanceId, shift ? '#f32' : '#3d8')
  }
})

Object.assign(window, { stage, THREE, tree })
