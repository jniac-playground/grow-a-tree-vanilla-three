import {
  BufferAttribute,
  Color,
  InstancedMesh,
  Vector3,
  Matrix4,
  Quaternion,
  Euler,
  BoxBufferGeometry,
  MeshPhysicalMaterial,
} from 'https://cdn.jsdelivr.net/npm/three@0.125.1/build/three.module.js'
import { enumerate, radians } from './utils/math.js'

const getBranch = ({
  x = 0,
  y = 0,
  z = 0,
  rx = 0,
  ry = 0,
  rz = 0,
  scale = 1,
}) => {
  const p = new Vector3(x, y + 1.5, z)
  const r = new Quaternion().setFromEuler(new Euler(...radians(rx, ry, rz)))
  const s = new Vector3(scale, scale, scale)
  const m = new Matrix4()
  return m.compose(p, r, s).multiply(new Matrix4().makeTranslation(0, 1.5, 0))
}

/**
 * From an index, get all child indexes (including the given index).
 * @param {Map<number, number[]>} graph 
 * @param {number} parentId 
 * @returns {Set<number>}
 */
const getChildrenIndexes = (graph, parentId) => {
  const children = new Set()
  if (parentId === -1)
    return children
  const current = [parentId]
  while (current.length > 0) {
    const id = current.pop()
    children.add(id)
    const childIds = graph.get(id)
    if (childIds) {
      current.push(...childIds)
    }
  }
  return children
}

/**
 * From an index, get all child indexes (including the given index).
 * @param {Map<number, number[]>} graph 
 * @param {number} parentId 
 * @returns {Set<number>}
 */
const getTipIndexes = (graph, parentId) => {
  const tips = new Set()
  const current = [parentId]
  while (current.length > 0) {
    const id = current.pop()
    const childIds = graph.get(id)
    if (childIds) {
      current.push(...childIds)
    } else {
      tips.add(id)
    }
  }
  return tips
}

/**
 * @returns {{
 *  matrices:Float32Array
 *  graph:Map<number,number[]>
 * }}
 */
const getInitialState = () => ({
  matrices: new Float32Array(new Matrix4().makeTranslation(0, 1.5, 0).elements),
  graph: new Map(),
})
 
export class InstancedBox extends InstancedMesh {

  constructor() {
    const geometry = new BoxBufferGeometry(0.5, 3, 0.5)
    const material = new MeshPhysicalMaterial({ color:'pink' })
    super(geometry, material, 0)

    this.instanceColor = new BufferAttribute(new Float32Array(0), 3)

    const { matrices, graph } = getInitialState()
    this.graph = graph
    
    this.setMatrices(matrices)
  }
  
  /**
   * Get the new matrices, for a given id.
   * Here is the design of the tree.
   * @param {number} id 
   */
  getNewMatrices(id) {
    // local matrices
    const m1 = getBranch({ rx:3, ry:45, rz:20, scale:.9 })
    const m2 = getBranch({ rx:-2, ry:35, rz:-20, scale:.8 })
    // the compute "tree" matrices
    const m = new Matrix4()
    this.getMatrixAt(id, m)
    return [
      m.clone().multiply(m1),
      m.clone().multiply(m2),
    ]
  }

  get matrices() { return this.instanceMatrix.array }

  /**
   * @param {Float32Array} matrices 
   */
  setMatrices(matrices) {
    const count = matrices.length / 16
    this.count = count
    this.instanceMatrix.array = matrices
    this.instanceMatrix.needsUpdate = true
    this.instanceColor.array = new Float32Array(count * 3)
    this.instanceColor.array.fill(1)
    this.instanceColor.needsUpdate = true
    console.log(count)
  }

  /**
   * Push new matrices elements into the buffer.
   * @param {Matrix4[]} newMatrices 
   */
  pushNewMatrices(newMatrices) {
    let { matrices } = this
    const oldMatrices = matrices
    const oldCount = matrices.length / 16
    matrices = new Float32Array((oldCount + newMatrices.length) * 16)
    matrices.set(oldMatrices, 0)
    for (const [index, matrix] of newMatrices.entries()) {
      matrices.set(matrix.elements, (oldCount + index) * 16)
    }
    this.setMatrices(matrices)
  }
  
  grow(instanceId = 0) {
    const { graph, matrices } = this
    let count = matrices.length / 16
    const newMatrices = []
    for (const id of getTipIndexes(graph, instanceId)) {
      const newMatricesIds = []
      for (const matrix of this.getNewMatrices(id)) {
        newMatricesIds.push(count)
        newMatrices.push(matrix)
        count++
      }
      graph.set(id, newMatricesIds)
    }
    this.pushNewMatrices(newMatrices)
  }

  prune(instanceId) {
    let { graph, matrices } = this
    const removed = getChildrenIndexes(graph, instanceId)
    const oldMatrices = matrices
    const oldCount = oldMatrices.length / 16
    const count = (oldMatrices.length / 16) - removed.size
    matrices = new Float32Array(count * 16)
    const oldToNew = new Map()
    // Looping over all matrices/nodes.
    // Removed indexes are removed from graph (delete).
    // "instanceId" is removed from the children array (that hold the value).
    // "matrices" is rebuilt from the kept indexes. 
    let index = 0
    for (const oldIndex of enumerate(oldCount)) {
      if (removed.has(oldIndex)) {
        graph.delete(oldIndex)
      } else {
        const children = graph.get(oldIndex)
        if (children && children.includes(instanceId)) {
          graph.set(oldIndex, children.filter(id => id !== instanceId))
        }
        oldToNew.set(oldIndex, index)
        matrices.set(oldMatrices.subarray(oldIndex * 16, (oldIndex + 1) * 16), index * 16)
        index += 1
      }
    }
    // Rebuild graph, from the old one, using the "oldToNew" index map.
    const oldGraph = graph
    graph = new Map()
    for (const [id, children] of oldGraph.entries()) {
      graph.set(oldToNew.get(id), children.map(child => oldToNew.get(child)))
    }
    
    this.graph = graph
    this.setMatrices(matrices)
  }

  /**
   * Change the color of the entire tree.
   * @param {string|number|Color} color 
   */
  setColor(color) {
    const { r, g, b } = new Color(color)
    const { array } = this.instanceColor
    const count = array.length / 3
    for (const index of enumerate(count)) {
      array[index * 3 + 0] = r
      array[index * 3 + 1] = g
      array[index * 3 + 2] = b
    }
  }

  /**
   * Change the color of an entire branch.
   * @param {number} instanceId 
   * @param {string|number|Color} color 
   */
  setBranchColor(instanceId, color) {
    const { r, g, b } = new Color(color)
    const { graph } = this
    const children = getChildrenIndexes(graph, instanceId)
    const { array } = this.instanceColor
    for (const index of children) {
      array[index * 3 + 0] = r
      array[index * 3 + 1] = g
      array[index * 3 + 2] = b
    }
    this.instanceColor.needsUpdate = true
  }
}