import * as THREE from 'https://cdn.jsdelivr.net/npm/three@0.125.1/build/three.module.js'
import { radians } from './utils/math.js'

export const wireSphere = ({ 
  radius = 40, 
  color = '#333',
  half = false,
} = {}) => {
  let heightSegments = 30
  let thetaLength = Math.PI
  if (half) {
    heightSegments /= 2
    thetaLength /= 2
  }
  return new THREE.Mesh(
    new THREE.SphereBufferGeometry(radius, 60, heightSegments, 0, Math.PI * 2, 0, thetaLength),
    new THREE.MeshBasicMaterial({ wireframe:true, color })
  )
}

export const pointLight = ({ 
  color = 'white', 
  intensity = 0.5,
  position = [20, 30, 10],
} = {}) => {
  const light = new THREE.PointLight(color, intensity)
  light.position.set(...position)
  return light
}

export const disc = ({
  radius = 5,
  position = [0, 0, 0],
} = {}) => {
  const mesh = new THREE.Mesh(
    new THREE.CircleBufferGeometry(radius, 64),
    new THREE.MeshLambertMaterial({ transparent:true, opacity:0.1 }),
  )
  mesh.rotation.set(...radians(-90, 0, 0))
  // mesh.position.set(position)
  return mesh
}